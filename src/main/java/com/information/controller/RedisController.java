package com.information.controller;


import com.information.util.RedisUtil;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("redis")
@Slf4j
public class RedisController {
    @Autowired
    private RedisUtil redisUtil;

    @RequestMapping("test01")
    void test01() throws InterruptedException {
        redisUtil.setCacheObject("name", "云影");
        redisUtil.expire("name", 60);
        System.out.println("成功缓存");
    }
    @RequestMapping("test02")
    void test02() throws InterruptedException {
        String name=redisUtil.getCacheObject("name");
        log.info("----{}--",name);
//        Thread.sleep(2000);  // 睡2秒
    }


}
